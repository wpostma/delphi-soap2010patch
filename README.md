# patch for delphi 2010 soaphttptrans.pas 


## notes 

Patch from Bruneau Babet, from long deleted forum post on Embarcadero forums.

 
> Hello,
>
> Below is a patched version of the *2010* version of SOAPHTTPTrans.pas
> to  resolve the problem described in this thread. As with the C++
> patch, I'll  illustrate the problem. You can see the problem in a
> simple Button Click  handler. For example:
 
 
```
    program  demo;
        // .. lots of boilerplate omitted 
       const
         Request =
         '<?xml version="1.0"?>' +
         '<SOAP-ENV:Envelope ' +
         ' xmlns:SOAP-ENV='+
         '"http://schemas.xmlsoap.org/soap/envelope/">' +
          ' <SOAP-ENV:Body>' +
          '  <x_Person xmlns="http://soapinterop.org/xsd" ' +
          '      Name="ЗАКУСКИ" ' +
          '      Male="true"> ' +
          '    <Age>45.5</Age>' +
          '    <ID>1234.5678</ID>' +
          '  </x_Person>' +
          ' </SOAP-ENV:Body>' +
          '</SOAP-ENV:Envelope>';
       
        const URL= 'http://mssoapinterop.org/asmx/wsdl/compound1.asmx';
       
        // .. lots of boilerplate omitted
       
        procedure TForm29.InvokeEchoPersonClick(Sender: TObject);
        var
          RR: THTTPReqResp;
          Response: TMemoryStream;
          U8: UTF8String;
        begin
          RR := THTTPReqResp.Create(nil);
          try
            RR.URL := URL;
            RR.UseUTF8InHeader := True;
            RR.SoapAction := 'http://soapinterop/echoPerson';
            Response := TMemoryStream.Create;
            try
              RR.Execute(Request, Response);
              SetLength(U8, Response.Size);
              Response.Position := 0;
              Response.Read(U8[1], Length(U8));
              ShowMessage(U8);
            finally
              Response.Free;
            end;
          finally
            RR.Free;
          end;
        end;
        {code}
```
       
    The above example invokes the EchoPerson service available here -
    http://mssoapinterop.org/asmx/wsdl/compound1.asmx. The response comes back
    nicely and is displayed in a MessageBox. Now, update the URL variable to
    something invalid, such as http://mssoapintero.org/asmx/wsdl/compound1.asmx.
    (NOTE: I've dropped the ending 'p' on the domain name). When you run you
   should get an error message about being unable to resolve the server name.
   Instead, you get an empty MessageBox. The issue is that the code failed to
   probably catch the fact that the HTTP POST failed. In this case, we're
    failing to connect. But let's say we have a case where we can connect but
   the Server is too busy and it cannot process the posted data. The 'Send'
   will fail. However, the runtime will fail to detect this and it will process
   to receiving the response - at which point WinInet will say "Handle is in
   the wrong state for the requested operation".
   
   So the core issue is the failure to detect a failed WinInet operation. The
   updated SOAPHTTPTrans.pas below remedies.
   
   Cheers,
   
   Bruneau
   
   